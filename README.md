############################
# Les Samedi Programmation #
############################


Samedi 18/07/2020 : Complexité algorithmique
============================================


Ce code source est la base du live Complexité algorithmique.


Il montre des exemples de code plus ou moins bien implémentés, avec des complexités diverses, pour remplir le même objectif.

- Complexité linéaire O(n)
- Complexité quadratique O(n²)
- Complexité factorielle O(n!)

Pour compiler et obtenir un exécutable, go build main.go

Si vous avez plus de questions, vous pouvez me contacter sur https://twitch.tv/adaralex

Bonne journée à vous,

Adaralex