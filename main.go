package main

import (
	"fmt"
	"strings"
	"time"
)

type CompteOccurence struct {
	Mot    string
	Compte int
}

func main() {

	// complexité = O(1)

	tableau := make([]int, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)
	tableau = append(tableau, 0)

	// Complexité O(1)
	fmt.Println(len(tableau))

	// Complexité O(n)
	counter := 0
	for _, _ = range tableau {
		counter = counter + 1
	}
	fmt.Println(counter)

	article := "Dernier blockbuster exclusif de la PlayStation 4 avant l'arrivée de la PS5, l'œuvre du studio américain Sucker Punch nous entraîne dans une vaste aventure samouraï. Plastiquement superbe, \"Ghost of Tsushima\" se révèle pourtant moins dépaysant que prévu. Et aussi : l'ovni \"Infini\", la bonne surprise \"Void Terrarium\", un \"Mr Driller\" inédit, le miraculé \"Ultracore\" (dont la sortie avait été annulée il y a 25 ans) et le flamboyant shoot'em up \"Sisters Royale\".\nEncore une mauvaise nouvelle pour la direction d’Ubisoft après la mise en lumière, grâce aux enquêtes publiées par Libération et par Numérama, des problèmes de sexisme et de harcèlement en vigueur au sein du groupe depuis des années. Si jamais, après l’épisode viking baptisé Valhalla et qui sortira à la fin de l’année, l’éditeur basé à Montreuil envisageait d’entraîner sa saga reine dans le Japon féodal, il pourrait devoir modifier ses plans, car l’Assassin’s Creed samouraï est déjà parmi nous. Si ce n'est qu’il est l’œuvre du studio américain Sucker Punch, connu jusqu’ici pour Sly Raccoon et inFamous, et qu’il a pour titre Ghost of Tsushima.\n\nSouligner tout ce que ce jeu, attendu comme le dernier blockbuster exclusif de la PlayStation 4 avant l’arrivée, pour les fêtes, de la relève PS5, partage avec la série d’Ubisoft n’a rien d’insultant. Au vu du niveau atteint par ses deux derniers volets en date, Assassin’s Creed Origins et Odyssey, on pourra même y voir un compliment. De fait, par l’ampleur et la densité de l’aventure qu’il propose comme par le soin sidérant apporté à son univers (ses villages, ses temples, ses ciels…), par la richesse, aussi, de ses systèmes de jeu qui contribue à en faire une expérience s’adaptant largement aux penchants et aux envies des joueurs, pressés ou flâneurs, contemplatifs ou bagarreurs, Ghost of Tsushima ressemble à l’idée que l’on peut se faire d’un très bon jeu vidéo à gros budget en 2020. Il coche à peu près toutes les cases et n’oublie rien ni personne en chemin (avec, notamment, de précieuses options d’\"accessibilité\"). Et, avec tout ça, on oserait faire la fine bouche ? "
	article = article + " Suivre le vent\nLe mot-clé est \"skin\". En jeu vidéo et plus généralement en informatique, le terme désigne l’habillage qui permet de modifier l’apparence d’un programme, d’un monde ou d’un personnage, notamment dans les jeux dits free-to-play type Brawl Stars ou Fortnite. De le déguiser, en somme, sans modifier ce qu’il est profondément. De ce point de vue, les créateurs de Ghost of Tsushima vont très loin puisque le principe de la \"skin\" s'y décline jusque dans le choix des modes de jeu où, en complément des options de langue (versions française, anglaise ou japonaise), de sous-titres ou des niveaux de difficulté, nous est proposé un \"mode Kurosawa\", conçu avec l’accord des héritiers du grand cinéaste japonais et dont le noir et blanc, le grain de l’image, mais aussi le son et même l’intensité du vent qui soufflent dans les herbes hautes rappellent l’esthétique de films comme Sanjuro, Rashomon ou Les Sept Samouraïs. Le résultat est très impressionnant. Troublant, même. Assassin’s Creed n’avait encore jamais ressemblé à ça.\n\nAlors, pris par l’ambiance, on y va. Sur notre cheval, on file d’un point à un autre de la carte pour aller accomplir une mission principale ou secondaire, pour enclencher un nouveau \"récit\" (du nom donné à certaines branches de l’histoire) ou simplement pour essayer de trouver l’un ou l’autre des multiples points d’intérêt savamment disséminés (sanctuaires, phares, sources clandestines…) Une jolie idée : ce n’est pas une flèche façon GPS qui, à l’écran, nous indique la direction à suivre, mais le vent lui-même (que l’on peine parfois à suivre, mais c’est peut-être notre sens de l’orientation déficient qu’il faut incriminer ici)."
	article = article + " Souvent, en cours de route, des ennemis se dressent devant nous que, selon les cas, on choisira ou non d’affronter. Mais, en général, mieux vaut y aller pour, une fois sorti vainqueur, faire progresser notre héros comme dans un jeu de rôle en gérant ses techniques, ses \"postures\" (que l'on peut comparer aux styles différents que proposent pour un même personnage certains jeux de combat) ou son équipement. Ne pas oublier non plus de ramasser fleurs, \"provisions\", fer et autres matériaux qui nous tombent sous la main : c’est grâce à eux que l’on améliorera notre katana, notre arc ou notre armure à condition de nous rendre au guichet – si l’on ose dire – correspondant. De temps en temps, on attaque des bases pour les libérer. Bon. Une idée, quand même : celle qu’assassiner quelqu’un en arrivant discrètement dans son dos n’est pas digne d’un samouraï, qui doit plutôt attaquer de face, les yeux dans les yeux, ce qui prend ici la forme de \"confrontations\" théâtralisées au cours desquelles, d’une manière symboliquement plutôt astucieuse, le jeu nous demande de lâcher un bouton (plutôt que de le presser) au moment voulu. En découle une légère tension entre ces deux approches, le duel samouraï (frontal) et l’attaque Assassin’s Creed (en douce, sans se faire repérer), alors que cette dernière, fourbe mais toujours efficace, demeure un élément essentiel du système de jeu."
	article = article + " Jouer à Ghost of Tsushima, c’est partir en voyage à l’autre bout du monde, s’émerveiller devant les superbes paysages, prendre plein de jolies photos et remplir ses bagages de souvenirs (un artefact ici, quelques haikus par là, une ou deux cages de criquets chantants…) et, pourtant, se dire qu’au-delà des signes, du pittoresque et des fétiches évidemment précieux, ce qu’on y fait et ce qu’on ressent n’est pas bien différent de nos vacances / du jeu d’avant. Sous la skin, sous la peau, ce sont les mêmes muscles, les mêmes os. Les mêmes pistons sous le capot. Ce qui ne veut pas dire qu’on n'y prend pas plaisir, qu’on ne vit pas de moments forts, mais ce splendide Japon médiéval a comme un arrière-goût de banal. Luxueuse et ambitieuse, la dernière grosse exclusivité de la PS4 est plutôt pas mal."
	article = article + " Suivre le vent\nLe mot-clé est \"skin\". En jeu vidéo et plus généralement en informatique, le terme désigne l’habillage qui permet de modifier l’apparence d’un programme, d’un monde ou d’un personnage, notamment dans les jeux dits free-to-play type Brawl Stars ou Fortnite. De le déguiser, en somme, sans modifier ce qu’il est profondément. De ce point de vue, les créateurs de Ghost of Tsushima vont très loin puisque le principe de la \"skin\" s'y décline jusque dans le choix des modes de jeu où, en complément des options de langue (versions française, anglaise ou japonaise), de sous-titres ou des niveaux de difficulté, nous est proposé un \"mode Kurosawa\", conçu avec l’accord des héritiers du grand cinéaste japonais et dont le noir et blanc, le grain de l’image, mais aussi le son et même l’intensité du vent qui soufflent dans les herbes hautes rappellent l’esthétique de films comme Sanjuro, Rashomon ou Les Sept Samouraïs. Le résultat est très impressionnant. Troublant, même. Assassin’s Creed n’avait encore jamais ressemblé à ça.\n\nAlors, pris par l’ambiance, on y va. Sur notre cheval, on file d’un point à un autre de la carte pour aller accomplir une mission principale ou secondaire, pour enclencher un nouveau \"récit\" (du nom donné à certaines branches de l’histoire) ou simplement pour essayer de trouver l’un ou l’autre des multiples points d’intérêt savamment disséminés (sanctuaires, phares, sources clandestines…) Une jolie idée : ce n’est pas une flèche façon GPS qui, à l’écran, nous indique la direction à suivre, mais le vent lui-même (que l’on peine parfois à suivre, mais c’est peut-être notre sens de l’orientation déficient qu’il faut incriminer ici)."
	article = article + " Souvent, en cours de route, des ennemis se dressent devant nous que, selon les cas, on choisira ou non d’affronter. Mais, en général, mieux vaut y aller pour, une fois sorti vainqueur, faire progresser notre héros comme dans un jeu de rôle en gérant ses techniques, ses \"postures\" (que l'on peut comparer aux styles différents que proposent pour un même personnage certains jeux de combat) ou son équipement. Ne pas oublier non plus de ramasser fleurs, \"provisions\", fer et autres matériaux qui nous tombent sous la main : c’est grâce à eux que l’on améliorera notre katana, notre arc ou notre armure à condition de nous rendre au guichet – si l’on ose dire – correspondant. De temps en temps, on attaque des bases pour les libérer. Bon. Une idée, quand même : celle qu’assassiner quelqu’un en arrivant discrètement dans son dos n’est pas digne d’un samouraï, qui doit plutôt attaquer de face, les yeux dans les yeux, ce qui prend ici la forme de \"confrontations\" théâtralisées au cours desquelles, d’une manière symboliquement plutôt astucieuse, le jeu nous demande de lâcher un bouton (plutôt que de le presser) au moment voulu. En découle une légère tension entre ces deux approches, le duel samouraï (frontal) et l’attaque Assassin’s Creed (en douce, sans se faire repérer), alors que cette dernière, fourbe mais toujours efficace, demeure un élément essentiel du système de jeu."
	article = article + " Jouer à Ghost of Tsushima, c’est partir en voyage à l’autre bout du monde, s’émerveiller devant les superbes paysages, prendre plein de jolies photos et remplir ses bagages de souvenirs (un artefact ici, quelques haikus par là, une ou deux cages de criquets chantants…) et, pourtant, se dire qu’au-delà des signes, du pittoresque et des fétiches évidemment précieux, ce qu’on y fait et ce qu’on ressent n’est pas bien différent de nos vacances / du jeu d’avant. Sous la skin, sous la peau, ce sont les mêmes muscles, les mêmes os. Les mêmes pistons sous le capot. Ce qui ne veut pas dire qu’on n'y prend pas plaisir, qu’on ne vit pas de moments forts, mais ce splendide Japon médiéval a comme un arrière-goût de banal. Luxueuse et ambitieuse, la dernière grosse exclusivité de la PS4 est plutôt pas mal."

	article = strings.ToLower(article)

	mots := strings.FieldsFunc(article, func(r rune) bool {
		return r == ' ' || r == '.' || r == '\'' || r == ',' || r == '’' || r == '(' || r == ')' || r == '"' || r == ':' || r == ';' || r == '\n'
	})

	occurences := make([]CompteOccurence, 0)

	// Complexité O(n²)

	fmt.Println("Début tableau (50 ms)")
	fmt.Println(time.Now())

	for _, mot := range mots { // complexité linéaire
		trouve := false
		for i, occurence := range occurences { // complexité linéaire
			if occurence.Mot == mot {
				occurence.Compte = occurence.Compte + 1
				occurences[i] = occurence
				trouve = true
			}
		}
		if !trouve {
			occurences = append(occurences, CompteOccurence{
				Mot:    mot,
				Compte: 1,
			})
		}
	}
	fmt.Println("Fin tableau")
	fmt.Println(time.Now())

	fmt.Printf("Mots différents dans l'article : %d\n", len(occurences))

	occurenceMap := make(map[string]int)

	fmt.Println("Début map (1 ms)")
	fmt.Println(time.Now())

	for _, mot := range mots { // complexité linéaire

		occurence, present := occurenceMap[mot] // complexité fixe = O(1)
		if present {
			occurenceMap[mot] = occurence + 1 // complexité fixe = O(1)
		} else {
			occurenceMap[mot] = 1 // complexité fixe = O(1)
		}

	}
	fmt.Println("Fin map")
	fmt.Println(time.Now())

	fmt.Printf("Mots différents dans l'article (via map) : %d\n", len(occurenceMap))

	occurences = make([]CompteOccurence, 0)

	// Complexité O(n!)

	fmt.Println("Début tableau recursif (110 ms)")
	fmt.Println(time.Now())

	for _, mot := range mots { // complexité linéaire * factorielle

		result := findOccurence(occurences, mot)
		if result == -1 {
			occurences = append(occurences, CompteOccurence{
				Mot:    mot,
				Compte: 1,
			})
		} else {
			occ := occurences[result]
			occ.Compte = occ.Compte + 1
			occurences[result] = occ
		}

	}
	fmt.Println("Fin tableau recursif")
	fmt.Println(time.Now())

	fmt.Printf("Mots différents dans l'article : %d\n", len(occurences))

}


func findOccurence(occurences []CompteOccurence, mot string) int {
	result := -1

	for i, occurence := range occurences { // Complexité O(n)

		if i == len(occurences)-1 && occurence.Mot == mot {
			return i
		}
	}

	if len(occurences) > 0 {
		return findOccurence(occurences[:len(occurences)-1], mot) // Complexité O(n-1)
	}

	return result
}
